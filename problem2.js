function problem2(inventory) {
  ans = [];
  if (inventory && inventory.length > 0) {
    ans.push(inventory[inventory.length - 1]);
  }
  return ans;
}

module.exports = problem2;
