function problem4(inventory) {
  let result = [];
  if (inventory && inventory.length > 0) {
    for (i = 0; i < inventory.length; i++) {
      result.push(inventory[i].car_year);
    }
  }
  return result;
}

module.exports = problem4;
