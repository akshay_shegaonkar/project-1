function problem5(inventory, year) {
  let olderCars = [];
  if (inventory && inventory.length > 0 && year) {
    for (car of inventory) {
      if (car.car_year < year) {
        olderCars.push(car);
      }
    }
  }
  return olderCars;
}

module.exports = problem5;
