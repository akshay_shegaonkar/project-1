function problem6(inventory) {
  const arr = ["Audi", "BMW"];
  let result = [];
  if (inventory && inventory.length > 0) {
    for (car of inventory) {
      if (arr.includes(car.car_make)) {
        result.push(car);
      }
    }
  }
  return result;
}

module.exports = problem6;
