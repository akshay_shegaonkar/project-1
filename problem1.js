function problem1(inventory, carId) {
  let ans = [];
  if (carId && inventory) {
    for (i = 0; i < inventory.length; i++) {
      if (inventory[i].id === carId) {
        ans.push(inventory[i]);
        break;
      }
    }
  }
  return ans;
}

module.exports = problem1;
