function problem3(inventory) {
  let models = [];
  if (inventory && inventory.length > 0) {
    for (i = 0; i < inventory.length; i++) {
      models.push(inventory[i].car_model);
    }
  }
  return models.sort();
}

module.exports = problem3;
